import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";
import {GoogleChartComponent} from "./js/angular-google-charts.js";

import { AppComponent } from './app.component';
import {AirSystemGraphComponent} from "./airSystemGraphs/airSystemGraph.component";
import { AirSystemDiagrammsComponent } from './air-system-diagramms/air-system-diagramms.component';
import { HeaderComponent } from './header/header.component';
import { AngularGoogleChartsComponent } from './angular-google-charts/angular-google-charts.component';


@NgModule({
  declarations: [
    AppComponent,
    AirSystemGraphComponent,
    AirSystemDiagrammsComponent,
    HeaderComponent,
    AngularGoogleChartsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
