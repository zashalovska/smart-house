import {Component} from "@angular/core";

@Component({
  selector: 'app-airSystemGraph',
  templateUrl: './airSystemGraph.component.html',
  styleUrls: ['./air-system-graphs.css']
})
export class AirSystemGraphComponent {
    systemNumber: number = 10;
    systemName: string = "";
    systemStatus: string =  "inactive";
    serverCreated = false;



    onClickServer(){
      this.systemStatus = "Server was created! System name is " + this.systemName;
      this.serverCreated = true;
    }

    onUpdateSystemName(event: any) {
      console.log(event);
      this.systemName = (<HTMLInputElement>event.target).value;
    }

}
