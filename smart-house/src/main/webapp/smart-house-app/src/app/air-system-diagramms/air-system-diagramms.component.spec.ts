import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AirSystemDiagrammsComponent } from './air-system-diagramms.component';

describe('AirSystemDiagrammsComponent', () => {
  let component: AirSystemDiagrammsComponent;
  let fixture: ComponentFixture<AirSystemDiagrammsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AirSystemDiagrammsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AirSystemDiagrammsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
