var currentLimit = 0;
var defaultLimit = 60;//default to last hour
var ppm_chart = null;
var ppm_gauge_chart = null;
var temp_chart = null;
var hum_chart = null;
var press_chart = null;
var temp_gauge_chart = null;
var press_gauge_chart = null;
var hum_gauge_chart = null;
var Promise = require("bluebird");

function drawGaugePPM(ppm) {
  ppm = parseInt(ppm);
  var data = google.visualization.arrayToDataTable([
    ['Label', 'Value'],
    ['CO2', ppm]
  ]);

  var options = {
    width: 250, height: 300,
    redFrom: 1400, redTo: 2000,
    yellowFrom: 1000, yellowTo: 1400,
    minorTicks: 10, max: 2000, min: 0, majorTicks: ["0", "400", "800", "1200", "1600", "2000"]
  };
  var d = document.getElementById('ppm_gauge_chart');

  if (ppm_gauge_chart)
    ppm_gauge_chart.clearChart();
  ppm_gauge_chart = new google.visualization.Gauge(d);
  ppm_gauge_chart.draw(data, options);

}

function drawDates(jsonData) {
  $(document).ready(function() {
    var interval = setInterval(function() {
      var momentNow = moment();
      $('#today_date').html(momentNow.format('YYYY-MM-DD') + ' '
        + momentNow.format('HH:mm:ss'));
    }, 100);
  });

  var current_date = new Date();
  var dateFormat = require('dateformat');
  dateFormat(current_date, "dddd, mmmm dS, yyyy, h:MM:ss TT");

  //$("#today_date").html(dateFormat(current_date, "yyyy-mm-dd H:MM"));
  $("#last_update_date").html(jsonData[0][0]);
}

function drawGaugeTemp(temp) {
  temp = parseInt(temp);
  var data = google.visualization.arrayToDataTable([
    ['Label', 'Value'],
    ['Temperature', temp]
  ]);

  var options = {
    width: 250, height: 300,
    redFrom: 40, redTo: 50,
    yellowFrom: 30, yellowTo: 40,
    minorTicks: 10, max: 50, min: 0, majorTicks: ["0", "10", "20", "30", "40", "50"]
  };
  var d = document.getElementById('temp_gauge_chart');

  if (temp_gauge_chart)
    temp_gauge_chart.clearChart();
  temp_gauge_chart = new google.visualization.Gauge(d);
  temp_gauge_chart.draw(data, options);

}

function drawGaugeHum(hum) {
  hum = parseInt(hum);
  var data = google.visualization.arrayToDataTable([
    ['Label', 'Value'],
    ['Humidity', hum]
  ]);

  var options = {
    width: 250, height: 300,
    redFrom: 60, redTo: 100,
    yellowFrom: 0, yellowTo: 30,
    minorTicks: 10, max: 100, min: 0, majorTicks: ["0", "20", "40", "60", "80", "100"]
  };
  var d = document.getElementById('hum_gauge_chart');

  if (hum_gauge_chart)
    hum_gauge_chart.clearChart();
  hum_gauge_chart = new google.visualization.Gauge(d);
  hum_gauge_chart.draw(data, options);

}

function drawGaugePress(press) {
    press = parseInt(press);
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['Pressure', press]
    ]);

    var options = {
        width: 250, height: 300,
        redFrom: 800, redTo: 850,
        yellowFrom: 650, yellowTo: 700,
        minorTicks: 10, max: 850, min: 650, majorTicks: ["650", "700", "750", "800", "850"]
    };


    var d = document.getElementById('press_gauge_chart');

    if (press_gauge_chart)
        press_gauge_chart.clearChart();
    press_gauge_chart = new google.visualization.Gauge(d);
    press_gauge_chart.draw(data, options);
    $( "#ppm_gauge_chart g > text:first" ).css( "font-size", "20" );
    $( "#press_gauge_chart g > text:first" ).css( "font-size", "20" );
    $( "#temp_gauge_chart g > text:first" ).css( "font-size", "20" );
    $( "#hum_gauge_chart g > text:first" ).css( "font-size", "20" );
}


function drawChartPPM(jsonData) {

  var data = new google.visualization.DataTable();
  data.addColumn('date', 'Date');
  data.addColumn('number', 'CO2 concentration, PPM');
  data.addColumn({type: 'string', role: 'tooltip'});
  data.addColumn('number', 'Warning level');
  data.addColumn('number', 'Danger level');
  $.each(jsonData, function (i, item) {
    var d1 = item[0].split(' ');
    var d = moment(item[0]).toDate();
    var tooltip = d1[1] + "\nPPM:" + item[1];
    data.addRows([[d, parseInt(item[1]), tooltip, 1000, 1400]]);
  });

  var options = {
    series: {
      0: {color: '#43459d'},
      1: {color: '#ffc870'},
      2: {color: '#e2431e'}
    },
    'chartArea': {'width': '80%', 'height': '80%'},
    title: 'CO2 concentration',
    curveType: 'function',
    legend: {position: 'bottom'},
    hAxis: {
      format: 'HH:mm',
      gridlines: {
        count: -1,
        units: {
          days: {format: ['MMM dd']},
          hours: {format: ['HH:mm']},
          minutes: {format: ['HH:mm']}
        }
      },
      minorGridlines: {
        units: {
          hours: {format: ['HH:mm:ss']},
          minutes: {format: ['HH:mm', 'mm']}
        }
      }

    }

  };

  if (ppm_chart)
    ppm_chart.clearChart();
  ppm_chart = new google.charts.Line(document.getElementById('ppm_chart'));

  ppm_chart.draw(data, options);
}

function drawChartHum(jsonData) {

  var data = new google.visualization.DataTable();
  data.addColumn('date', 'Date');
  data.addColumn('number', 'Humidity, %');
  data.addColumn({type: 'string', role: 'tooltip'});
  $.each(jsonData, function (i, item) {
    var d1 = item[0].split(' ');
    var d = moment(item[0]).toDate();
//console.log(d,item.ram)
    var tooltip = d1[1] + "\nHumidity:" + item[1];
    data.addRows([[d, parseInt(item[1]), tooltip]]);
  });

  var options = {
    series: {
      0: {color: '#43459d'},
      1: {color: '#ffc870'},
      2: {color: '#e2431e'}
    },
    'chartArea': {'width': '80%', 'height': '80%'},
    title: 'Humidity',
    curveType: 'function',
    legend: {position: 'bottom'},
    hAxis: {
      format: 'HH:mm',
      gridlines: {
        count: -1,
        units: {
          days: {format: ['MMM dd']},
          hours: {format: ['HH:mm']},
          minutes: {format: ['HH:mm']}
        }
      },
      minorGridlines: {
        units: {
          hours: {format: ['HH:mm:ss']},
          minutes: {format: ['HH:mm', 'mm']}
        }
      }
    }

  };

  if (hum_chart)
    hum_chart.clearChart();
  hum_chart = new google.charts.Line(document.getElementById('hum_chart'));

  hum_chart.draw(data, options);
}


function drawChartTemp(jsonData) {

  var data = new google.visualization.DataTable();
  data.addColumn('date', 'Date');
  data.addColumn('number', 'Temperature, C');
  data.addColumn({type: 'string', role: 'tooltip'});
  $.each(jsonData, function (i, item) {
    var d1 = item[0].split(' ');
    var d = moment(item[0]).toDate();
    var tooltip = d1[1] + "\nTemperature:" + item[1];
    data.addRows([[d, parseInt(item[1]), tooltip]]);
  });

  var options = {
    series: {
      0: {color: '#43459d'},
      1: {color: '#ffc870'},
      2: {color: '#e2431e'}
    },
    'chartArea': {'width': '80%', 'height': '80%'},
    title: 'Temperature, C',
    curveType: 'function',
    legend: {position: 'bottom'},
    hAxis: {
      format: 'HH:mm',
      gridlines: {
        count: -1,
        units: {
          days: {format: ['MMM dd']},
          hours: {format: ['HH:mm']},
          minutes: {format: ['HH:mm']}
        }
      },
      minorGridlines: {
        units: {
          hours: {format: ['HH:mm:ss']},
          minutes: {format: ['HH:mm', 'mm']}
        }
      }
    }

  };

  if (temp_chart)
    temp_chart.clearChart();
  temp_chart = new google.charts.Line(document.getElementById('temp_chart'));

  temp_chart.draw(data, options);
}

function drawChartPress(jsonData) {

    var data = new google.visualization.DataTable();
    data.addColumn('date', 'Date');
    data.addColumn('number', 'Pressure, mmHg');
    data.addColumn({type: 'string', role: 'tooltip'});
    $.each(jsonData, function (i, item) {
        var d1 = item[0].split(' ');
        var d = moment(item[0]).toDate();
        var tooltip = d1[1] + "\nPressure:" + item[1];
        data.addRows([[d, parseInt(item[1]), tooltip]]);
    });

    var options = {
        series: {
            0: {color: '#43459d'},
            1: {color: '#ffc870'},
            2: {color: '#e2431e'}
        },
        'chartArea': {'width': '80%', 'height': '80%'},
        title: 'Pressure',
        curveType: 'function',
        legend: {position: 'bottom'},
        hAxis: {
            format: 'HH:mm',
            gridlines: {
                count: -1,
                units: {
                    days: {format: ['MMM dd']},
                    hours: {format: ['HH:mm']},
                    minutes: {format: ['HH:mm']}
                }
            },
            minorGridlines: {
                units: {
                    hours: {format: ['HH:mm:ss']},
                    minutes: {format: ['HH:mm', 'mm']}
                }
            }
        }

    };

    if (press_chart)
        press_chart.clearChart();
    press_chart = new google.charts.Line(document.getElementById('press_chart'));
    press_chart.draw(data, options);
}

function setLimit(newLimit) {

  if (typeof newLimit === 'undefined')
    newLimit = 60;
  if (currentLimit !== newLimit) {
    var loader = '<div class="progress">' +
      '<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">'
      + '<span class="sr-only">45% Complete</span>'
      + '</div>' +
      '</div>';
    $("#ppm_chart").html(loader);
    $("#temp_chart").html(loader);
    $("#hum_chart").html(loader);
    $("#press_chart").html(loader);

    currentLimit = newLimit;
    redraw();
  }
}

function formatError(txt) {
  return '<div class="alert alert-danger">' + txt + '</div>';
}

var err_nodata = formatError('No data');

function redrawPPM() {
  console.log("redrawPPM");
  return $.ajax({
    url: "http://x-air-pro2-x-air-pro2.1d35.starter-us-east-1.openshiftapps.com/getAirSystems/" + currentLimit + "/ppm",
     // url: "http://localhost:8080/getAirSystems/" + currentLimit + "/ppm",
    dataType: "json"
  }).then(function (data) {
    if (data.length) {
      drawDates(data);
      drawGaugePPM(data[0][1]);
      drawChartPPM(data);
    }
    else {
      if (ppm_chart)
        ppm_chart.clearChart();
      if (ppm_gauge_chart)
        ppm_gauge_chart.clearChart();
      $("#ppm_gauge_chart").html(err_nodata);
      $("#ppm_chart").html(err_nodata);
    }
  }).catch(function (error) {
    $("#ppm_chart").append(formatError(error));
    $("#ppm_gauge_chart").html(formatError(error));
  });

}

function redrawTemp() {
  console.log("redrawTemp");
  return $.ajax({
    url: "http://x-air-pro2-x-air-pro2.1d35.starter-us-east-1.openshiftapps.com/getAirSystems/" + currentLimit + "/temp",
   //   url: "http://localhost:8080/getAirSystems/" + currentLimit + "/temp",
    dataType: "json"
  }).then(function (data) {
    if (data.length) {
      drawGaugeTemp(data[0][1]);
      drawChartTemp(data);
    }
    else {
      if (temp_chart)
        temp_chart.clearChart();
      if (temp_gauge_chart)
        temp_gauge_chart.clearChart();
      $("#temp_gauge_chart").html(err_nodata);
      $("#temp_chart").html(err_nodata);
    }
  }).catch(function (error) {
    $("#temp_chart").html(formatError(error));
    $("#temp_gauge_chart").html(formatError(error));
  });
}

function redrawHumidity() {
  console.log("redrawHumidity");
  return $.ajax({
    url: "http://x-air-pro2-x-air-pro2.1d35.starter-us-east-1.openshiftapps.com/getAirSystems/" + currentLimit + "/humidity",
    //  url: "http://localhost:8080/getAirSystems/" + currentLimit + "/humidity",
    dataType: "json"
  }).then(function (data) {
    if (data.length) {
      drawGaugeHum(data[0][1]);
      drawChartHum(data);
    }
    else {
      if (hum_chart)
        hum_chart.clearChart();
      if (hum_gauge_chart)
        hum_gauge_chart.clearChart();
      $("#hum_gauge_chart").html(err_nodata);
      $("#hum_chart").html(err_nodata);
    }
  }).catch(function (error) {
    $("#hum_chart").html(formatError(error));
    $("#hum_gauge_chart").html(formatError(error));
  });
}

function redrawPressure() {
    return $.ajax({
        url: "http://x-air-pro2-x-air-pro2.1d35.starter-us-east-1.openshiftapps.com/getAirSystems/" + currentLimit + "/press",
       // url: "http://localhost:8080/getAirSystems/" + currentLimit + "/press",
        dataType: "json"
    }).then(function (data) {
        if (data.length) {
            drawGaugePress(data[0][1]);
            drawChartPress(data);
        }
        else {
            if (press_chart)
                press_chart.clearChart();
            if (press_gauge_chart)
                press_gauge_chart.clearChart();
            $("#press_gauge_chart").html(err_nodata);
            $("#press_chart").html(err_nodata);
        }
    }).catch(function (error) {
        $("#press_chart").html(formatError(error));
        $("#press_gauge_chart").html(formatError(error));
    });
}

function redraw() {
  return Promise.reduce([redrawPPM, redrawTemp, redrawHumidity, redrawPressure], function (res, updateRes) {
    return updateRes();
  }, Promise.resolve());
}

function redrawLoop() {
  redraw().delay(60000)
    .then(function () {
      redrawLoop();
    });
}

$(function () {
  setLimit(defaultLimit);

  $("#btn-hour").click(function () {
    setLimit(60)
  });
  $("#btn-day").click(function () {
    setLimit(60 * 24)
  });
  $("#btn-week").click(function () {
    setLimit(60 * 24 * 7)
  });
  $("#btn-2weeks").click(function () {
    setLimit(60 * 24 * 14)
  });
  $("#btn-month").click(function () {
    setLimit(60 * 24 * 30)
  });
  google.charts.load('visualization', '45', {
    callback: redrawLoop,
    packages: ['line', 'corechart', 'gauge']
  });
});
